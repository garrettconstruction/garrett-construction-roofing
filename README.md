Garrett Construction is a licensed General Contractor. Our general services range from full residential and commercial construction projects to remodeling, painting, drywall, roofing and gutters. We also help with disaster recovery with experienced teams available to address fire and water damage.

Address: 912 Crown Ridge Lane, Powell, TN 37849, USA

Phone: 865-454-7531

Website: http://garrettroofingknoxville.com/
